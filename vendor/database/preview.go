package database

import (
	"github.com/jinzhu/gorm"
	"model"
	//postgres connect
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

//Preview - ORM struct for Preview Page
type Preview struct {
	db *gorm.DB
}

//Add - Preview Add book
func (p *Preview) Add(dPreview *model.DataPreview) {
	p.db.Create(&dPreview)
}

//QueryNew - Preview Query New
func (p *Preview) QueryNew() *model.DataPreview {
	var dPreview model.DataPreview
	p.db.First(&dPreview)
	return &dPreview
}

//QueryR18 - Preview Query R18 Grade
func (p *Preview) QueryR18(offset int) *model.DataPreview {
	var dPreview model.DataPreview
	p.db.Offset(offset).Where("Grade = ?", "R18").Find(dPreview)
	return &dPreview
}

//QueryR15 - Preview Query R15 Grade
func (p *Preview) QueryR15(offset int) *model.DataPreview {
	var dPreview model.DataPreview
	p.db.Offset(offset).Where("Grade = ?", "R15").Find(dPreview)
	return &dPreview
}
