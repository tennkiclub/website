package model

import (
	"github.com/jinzhu/gorm"
)

//DataPreview - a model of Online Preview
type DataPreview struct {
	gorm.Model
	URL        string `gorm:"unique"`
	Title      string
	PublicTime int `gorm:"primary_key"`
	Grade      string
	Tag        string
	PImg       string
	Content    string
	ImgJSON    string
}
