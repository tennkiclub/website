package model

import (
	"github.com/jinzhu/gorm"
)

//DataInfo model struct
type DataInfo struct {
	gorm.Model
	URL     string `gorm:"unique"`
	Title   string
	Content string
}
