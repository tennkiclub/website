// Package main provides ...
package main

import (
	"database"
	"endpoints"
	"fmt"
	"github.com/gin-gonic/gin"
	eztemplate "github.com/michelloworld/ez-gin-template"
	"middleware"
	"os"
)

func main() {
	if len(os.Args) > 1 {
		if os.Args[1] == "--runserver" {
			gin.SetMode(gin.ReleaseMode)
			route := gin.Default()
			route.Static("/static", "./static")
			route.Static("/public", "./public")
			render := eztemplate.New()
			render.TemplatesDir = "template/"
			render.Ext = ".tmpl"
			render.Debug = true
			route.HTMLRender = render.Init()
			route.GET("/", endpoints.Index)
			route.GET("/test", endpoints.Test)
			route.GET("/profile", endpoints.Profile)
			route.GET("/read", endpoints.Read)
			route.GET("/oauth/callback", middleware.AuthGithubCallback(), endpoints.OauthCallback)
			adminGroup := route.Group("/admin")
			adminGroup.Use(middleware.AuthRequired())
			{
				adminGroup.GET("/", endpoints.AdminIndex)
			}
			route.GET("/info/:url", endpoints.Information)
			route.Run()
		} else if os.Args[1] == "--dbinit" {
			dbinit := database.New()
			dbinit.CreateSchema()
			dbinit.CloseDBConnect()
		} else if os.Args[1] == "--dbtest" {
			dbtest := database.New()
			TestInfoAdd(dbtest, 10)
			TestProfileAdd(dbtest)
		} else {
			fmt.Println("Please use:")
			fmt.Println("--runserver : run gin Server. ")
			fmt.Println("--dbinit : run postgreSQL init.  ")
			os.Exit(3)
		}
	} else {
		fmt.Println("Please use:")
		fmt.Println("--runserver : run gin Server. ")
		fmt.Println("--dbinit : run postgreSQL init.  ")
		os.Exit(3)
	}
}
