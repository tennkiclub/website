package main

import (
	"database"
	"math/rand"
	"model"
	"strconv"
	"time"
)

//TestInfoAdd - Add DB
func TestInfoAdd(db *database.Service, count int) {
	db.CreateSchema()
	rand.Seed(time.Now().UnixNano())
	testinfo := model.DataInfo{URL: strconv.Itoa(rand.Intn(65535)), Title: "test", Content: "testit"}
	db.Information.Add(&testinfo)
	db.CloseDBConnect()
}

//TestProfileAdd - Add Profile
func TestProfileAdd(db *database.Service) {
	db.CreateSchema()
	testprofile := model.DataProfile{Icon: "/static/img/icon/mhh.jpg", Name: "棉花", Content: "所有的形象圖,社團角色,插圖以及東方電腦合同之主要執筆者<br/>熟悉插畫/漫畫以及各種不同風格之作畫 <br />上至百合下至BL 都難不倒他", Job: "繪師"}
	//testprofile := model.DataProfile{Icon: "/static/img/icon/0mu.jpg", Name: "零夢", Content: "沒什麼反應就是一位攤主(? <br /> 偶爾幹蠢事被社團的各位嗆 <br /> 平時負責網站維護,印刷,跟別攤裝逼（？", Job: "攤主/被嗆的"}
	db.Profile.Update(&testprofile)
	db.CloseDBConnect()
}

//TestPreviewAdd - Add Preview
func TestPreviewAdd(db *database.Service) {
	db.CreateSchema()
}
